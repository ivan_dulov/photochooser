﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;

namespace PhotoChooser
{
    class PhotoManager
    {
        public List<string> ChoosenPhotoFileNames { get; set; } = new List<string>();
        public string InitialDirectoryPath { get; set; }
        string outputDirectoryPath;

        public async void LoadInitialPhotosList()
        {
            InitialDirectoryPath=OpenFolderDialog();
            var task = new Task (()=>FillInitialPhotosList());
            task.Start();
            await task;
        }

        string OpenFolderDialog()
        {
            using (var folderBrowserDialog = new FolderBrowserDialog())
            {
                DialogResult result = folderBrowserDialog.ShowDialog();

                if (result == DialogResult.OK)
                    return folderBrowserDialog.SelectedPath;
                return null;
            }
        }

        void FillInitialPhotosList()
        {
            ChoosenPhotoFileNames.Clear();

            if (string.IsNullOrEmpty(InitialDirectoryPath))
                return;

            var photos = Directory.EnumerateFiles(InitialDirectoryPath, "*.jpg*");

            foreach (var photo in photos)
                ChoosenPhotoFileNames.Add(Path.GetFileName(photo));
        }

        public BitmapImage GetPhoto(string photoName)
        {
            BitmapImage photo = new BitmapImage();
            photo.BeginInit();
            var pathToPhotoFile = Path.Combine(InitialDirectoryPath, photoName);
            photo.UriSource = new Uri(pathToPhotoFile);
            photo.EndInit();
            return photo;
        }

        public async void SavePhotosList(List<string> photosNames)
        {
            outputDirectoryPath = OpenFolderDialog();

            if (MessageBox.Show($"Скопировать выбранные фото в папку {outputDirectoryPath}?", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            
            var task = new Task(() =>
            {
                foreach (var s in photosNames)
                {
                    Tuple<string, string> fromAndWhereFilePair = FromAndWhereFilePair(s);
                    File.Copy(fromAndWhereFilePair.Item1, fromAndWhereFilePair.Item2);
                }
            });

            task.Start();
            await task;

            MessageBox.Show("Копирование файлов успешно завершено", "", MessageBoxButtons.OK
                , MessageBoxIcon.Information);
        }

        Tuple<string, string> FromAndWhereFilePair(string fileName)
        {
            string fromFilePath = Path.Combine(InitialDirectoryPath, fileName).ToString();
            string whereFilePath = Path.Combine(outputDirectoryPath, fileName).ToString();

            Tuple<string, string> result = new Tuple<string, string>(fromFilePath, whereFilePath);

            return result;
        }
    }
}
