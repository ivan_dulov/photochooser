﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace PhotoChooser
{
    class ViewModel : INotifyPropertyChanged
    {
        public string StartDirectoryName { get; set; }
        public string OutputDirectoryName { get; set; }

        public string InitialPhotoDirectoryName
        {
            get => photoManager.InitialDirectoryPath;
            set => photoManager.InitialDirectoryPath = value;
        }
        public BitmapImage CurrentPhoto { get; set; }

        public string CurrentPhotoName {
            get => CurrentPhoto==null?string.Empty:
                System.IO.Path.GetFileName(CurrentPhoto.UriSource.Segments.Last());
        }

        string currentInitialPhotoName;
        public string CurrentInitialPhotoName {
            get => currentInitialPhotoName;
            set 
            {
                currentInitialPhotoName = value;
                SetCurrentPhoto(currentInitialPhotoName);
            }  
        }

        string currentSelectedPhotoName;

        public string CurrentSelectedPhotoName
        {
            get => currentSelectedPhotoName;
            set
            {
                currentSelectedPhotoName = value;
                SetCurrentPhoto(currentSelectedPhotoName);
            }
        }
        public ObservableCollection<string> InintilalPhotosList { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> SelectedPhotosList { get; set; } = new ObservableCollection<string>();

        public ICommand OpenInitialPhotoDirectoryCommand { get; set; }
        public ICommand OpenNextPhotoCommand { get; set; }
        public ICommand OpenPreviousPhotoCommand { get; set; }
        public ICommand MoveSelectedPhotoCommand { get; set; }
        public ICommand SaveSelectedListCommand { get; set; }
        public ICommand RemoveFromSelectedListCommand { get; set; }
        public ICommand ClearSelectedListCommand { get; set; }
        public bool IsNextPhotoButtonActive { get; set; } = false;
        public bool IsPreviousPhotoButtonActive { get; set; } = false;
        public bool IsMoveButtonActive { get; set; } = false;

        readonly PhotoManager photoManager = new PhotoManager();

        public ViewModel()
        {
            SetCommands();
        }

        void SetCommands()
        {
            OpenInitialPhotoDirectoryCommand = new RelayCommand(p => true, a => OpenInitialPhotosDirectory());
            OpenNextPhotoCommand = new RelayCommand(p => true, a => SelectNextPhoto());
            OpenPreviousPhotoCommand = new RelayCommand(p => true, a => SelectPreviousPhoto());
            MoveSelectedPhotoCommand = new RelayCommand(p => true, a => MoveSelectedPhoto());
            SaveSelectedListCommand = new RelayCommand(p => true, a => SaveSelectedList());
            RemoveFromSelectedListCommand = new RelayCommand(p => true, a => RemoveFromSelectedList());
            ClearSelectedListCommand = new RelayCommand(p => true, a => ClearSelectedList());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void OpenInitialPhotosDirectory()
        {
            InintilalPhotosList.Clear();
            photoManager.LoadInitialPhotosList();
            OnPropertyChanged(nameof(InitialPhotoDirectoryName));
            FillInitilaPhotosListFromModel();
        }

        void FillInitilaPhotosListFromModel()
        {
            if (photoManager.ChoosenPhotoFileNames.Count == 0)
            {
                MessageBox.Show("Выбранная папка не содержит фото!", "", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            foreach (var photo in photoManager.ChoosenPhotoFileNames)
                InintilalPhotosList.Add(photo);

            CurrentInitialPhotoName = InintilalPhotosList.First();
            IsMoveButtonActive = true;
            OnPropertyChanged(nameof(IsMoveButtonActive));
        }

        void SelectNextPhoto()
        {
            if (InintilalPhotosList.IndexOf(CurrentInitialPhotoName) == (InintilalPhotosList.Count()-1))
                return;

            string nextPhotoName = InintilalPhotosList[InintilalPhotosList.IndexOf(CurrentInitialPhotoName) + 1];
            CurrentInitialPhotoName = nextPhotoName;
            OnPropertyChanged(nameof(CurrentInitialPhotoName));
        }

        void SelectPreviousPhoto()
        {
            if (InintilalPhotosList.IndexOf(CurrentInitialPhotoName) == 0 || CurrentInitialPhotoName == null)
                return;

            string previousPhotoName = InintilalPhotosList[InintilalPhotosList.IndexOf(CurrentInitialPhotoName) - 1];
            CurrentInitialPhotoName = previousPhotoName;
            OnPropertyChanged(nameof(CurrentInitialPhotoName));

        }

        void MoveSelectedPhoto()
        {
            if (SelectedPhotosList.Contains(currentInitialPhotoName))
                return;

            SelectedPhotosList.Add(currentInitialPhotoName);
        }

        void SaveSelectedList()
        {
            photoManager.SavePhotosList(SelectedPhotosList.ToList());
        }

        void RemoveFromSelectedList()
        {
            SelectedPhotosList.Remove(CurrentSelectedPhotoName);
        }

        private void ClearSelectedList()
        {
            SelectedPhotosList.Clear();
        }

        void SetCurrentPhoto(string photoName)
        {
            CurrentPhoto = photoManager.GetPhoto(photoName);
            OnPropertyChanged(nameof(CurrentPhoto));
            OnPropertyChanged(nameof(CurrentPhotoName));
            SetButtonsState();
        }

        void SetButtonsState()
        {
            if (currentInitialPhotoName == InintilalPhotosList.First())
                IsPreviousPhotoButtonActive = false;
            else
                IsPreviousPhotoButtonActive = true;

            OnPropertyChanged(nameof(IsPreviousPhotoButtonActive));

            if (currentInitialPhotoName == InintilalPhotosList.Last())
                IsNextPhotoButtonActive = false;
            else
                IsNextPhotoButtonActive = true;

            OnPropertyChanged(nameof(IsNextPhotoButtonActive));
        }

        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
